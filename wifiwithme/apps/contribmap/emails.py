import datetime

import pytz
from django.conf import settings
from django.core.mail import send_mail
from django.utils.translation import activate


def _get_contrib_context(contrib, mgmt_token, request=None):
    """ Sends an email related to a contrib
    """
    activate(settings.LANGUAGE_CODE)
    if request:
        permalink = contrib.get_absolute_url(request)
    else:
        permalink = None

    ndays = (contrib.expiration_date - datetime.datetime.now(pytz.utc)).days

    return {
        'contrib': contrib,
        'ndays': ndays,
        'isp': settings.ISP,
        'management_link': contrib.make_management_url(mgmt_token),
        'mgmt_token': mgmt_token,
        'permalink': permalink,
    }


def send_contributor_email(
        contrib, subject_template, body_template, mgmt_token, request=None):
    context = _get_contrib_context(contrib, mgmt_token, request)
    send_mail(
        subject_template.render(context),
        body_template.render(context),
        settings.DEFAULT_FROM_EMAIL,
        [contrib.email],
    )
    return context


def send_moderator_emails(
        contrib, subject_template, body_template, mgmt_token, request):
    context = _get_contrib_context(contrib, mgmt_token, request)
    if len(settings.NOTIFICATION_EMAILS) > 0:
        send_mail(
            subject_template.render(context),
            body_template.render(context),
            settings.DEFAULT_FROM_EMAIL,
            settings.NOTIFICATION_EMAILS,
        )
    return context
