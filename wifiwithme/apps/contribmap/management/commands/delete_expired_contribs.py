"""Delete expired contributions

Based on the expiration date that exists on any contrib. Produce no output by
default (cron stuff)

"""

from django.conf import settings
from django.core.management.base import BaseCommand

from ...models import Contrib


class Command(BaseCommand):
    help = __doc__

    def add_arguments(self, parser):
        parser.add_argument(
            '--dry-run', default=False, action='store_true',
            help="Do not actually delete contributions, just print which ones should be")

    def handle(self, dry_run, *args, **options):
        if dry_run:
            self.stderr.write('DRY RUN MODE: we do not actually delete contributions.\n')
        for contrib in Contrib.objects.expired():
            if not dry_run:
                contrib.delete()
            else:
                self._log_deleted_contrib(contrib)

    def _log_deleted_contrib(self, contrib):
        self.stderr.write("Would delete expired contribution {}\n".format(contrib))
