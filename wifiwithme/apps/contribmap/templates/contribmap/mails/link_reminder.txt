Chèr·e {{ contrib.name }},

Vous aviez déposé le {{ contrib.date|date:'j F o' }} une demande de raccordement.

Ceci est un rappel du lien qui vous permet de gérer votre demande (supprimer ou
prolonger d'un an).

<{{ management_link }}>

Sans intervention de votre part, votre demande, ainsi que les informations
personnelles associées seront supprimés de nos serveurs dans {{ ndays }} jours :
le **{{contrib.expiration_date|date }}**{% if isp.CNIL.LINK %}, conformément à notre déclaration CNIL¹{% endif %}.

Bien à vous,

Les bénévoles de {{ isp.NAME }}

{% if isp.CNIL.LINK %}¹ {{ isp.CNIL.LINK }}{% endif %}
