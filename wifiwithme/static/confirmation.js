$(document).ready(function() {
  /* If JS is enabled, add a confirmation box.
   * If not, delete without confirmation.
   */
  $('.confirmation-protected')
  .attr('type', 'button')
  .click(function(evt) {
    var confirmed = confirm(
      'Êtes-vous certain·e de vouloir supprimer votre contribution ?')
    if (confirmed) {
      $(evt.target).parent('form').submit();
    }
  });
});